Review Bundle
==========================

Библиотека для интеграции с Review Service.

## Документация

### Установка

Следует добавить:

composer.json
```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://ValentinDk@bitbucket.org/ValentinDk/review-bundle.git"
    }
  ]
}
```

```php
// config/bundles.php
return [
    // ...
    Slivki\Bundle\ReviewBundle\DependencyInjection\ReviewBundle::class => ['all' => true],
    // ...
];
```

```yaml
# config/packages/review.yaml
review:
  username: '%env(REVIEW_SERVICE_USERNAME)%'
  password: '%env(REVIEW_SERVICE_PASSWORD)%'
  base_uri: '%env(REVIEW_SERVICE_BASE_URI)%'
```

``` dotenv
# .env.local
REVIEW_SERVICE_USERNAME=test
REVIEW_SERVICE_PASSWORD=test
REVIEW_SERVICE_BASE_URI=http://test.test
```

Выполнить консольную команду:

```console
$ composer require slivki/review-bundle
```

### Применение

Отзывы:

```php
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateCompanyReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateListProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\RemoveReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Request\EditReviewWithReviewerTokenRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\CompanyReviewQueryString;
use Slivki\Bundle\ReviewBundle\Request\Query\ProductReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;

interface ReviewClientServiceInterface
{
    public function getCompanyReviews(CompanyReviewQueryString $queryString, string $companyId): ReviewCollectionResponse;

    public function createCompanyReview(CreateCompanyReviewRequestDto $requestDto): void;

    public function getProductReviews(ProductReviewQueryString $queryString, string $productId): ReviewCollectionResponse;

    public function createProductReview(CreateProductReviewRequestDto $requestDto): void;

    public function createListProductReview(CreateListProductReviewRequestDto $requestDto): void;

    public function edit(EditReviewWithReviewerTokenRequest $request, int $reviewId): void;

    public function remove(RemoveReviewRequestDto $requestDto, int $reviewId): void;
}
```

Комментарии:

```php
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\CreateCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\RemoveCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;

interface CommentClientServiceInterface
{
    public function getComments(CommentQueryString $queryString, int $reviewId): CommentCollectionResponse;

    public function create(CreateCommentRequestDto $requestDto): void;

    public function edit(EditCommentRequestDto $requestDto, int $commentId): void;

    public function remove(RemoveCommentRequestDto $requestDto, int $commentId): void;
}
```

Лайки:

```php
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;

interface LikeClientServiceInterface
{
    public function create(LikeRequestDto $requestDto): void;

    public function delete(LikeRequestDto $requestDto): void;
}
```

Дизлайки:

```php
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;

interface DislikeClientServiceInterface
{
    public function create(LikeRequestDto $requestDto): void;

    public function delete(LikeRequestDto $requestDto): void;
}
```

#### Административная часть

Отзывы:

```php
use Slivki\Bundle\ReviewBundle\Request\EditReviewRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\BaseReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;

interface ReviewClientServiceInterface
{
    public function getNewReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse;

    public function getRemovedReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse;

    public function edit(EditReviewRequest $request, int $reviewId): void;

    public function remove(int $reviewId): void;

    public function read(int $reviewId): void;
}
```

Комментарии:

```php
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;

interface CommentClientServiceInterface
{
    public function getRemovedComments(CommentQueryString $queryString): CommentCollectionResponse;

    public function edit(EditCommentRequestDto $requestDto, int $commentId): void;

    public function remove(int $commentId): void;

    public function read(int $commentId): void;
}
```

#### Request и QueryString с необязательными полями:

```php
final class EditReviewWithReviewerTokenRequest
{
    // Свойства, которые не инициализируются в конструкторе, инициализировать, если значение изменилось
    // $request = new EditReviewWithReviewerTokenRequest('testToken);
    // $request->rating = 5;
    public string $reviewerToken; 
    public string $name;
    public int $rating;

    /** @var string[] */
    public array $imageUrls;

    public function __construct(string $reviewerToken)
    {
        $this->reviewerToken = $reviewerToken;
    }
}
```