<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class LikeClientService implements LikeClientServiceInterface
{
    private HttpClientInterface $client;
    private string $token;

    public function __construct(HttpClientInterface $client, UserAuthenticatorInterface $authenticator)
    {
        $this->client = $client;
        $this->token = $authenticator->auth();
    }

    public function create(LikeRequestDto $requestDto): void
    {
        $this->client->request(
            Request::METHOD_POST,
            '/api/like',
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $requestDto,
            ]
        )->getContent();
    }

    public function delete(LikeRequestDto $requestDto): void
    {
        $this->client->request(
            Request::METHOD_DELETE,
            '/api/like',
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $requestDto,
            ]
        )->getContent();
    }
}
