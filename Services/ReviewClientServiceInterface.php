<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateCompanyReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateListProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\RemoveReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Request\EditReviewWithReviewerTokenRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\CompanyReviewQueryString;
use Slivki\Bundle\ReviewBundle\Request\Query\ProductReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;

interface ReviewClientServiceInterface
{
    public function getCompanyReviews(CompanyReviewQueryString $queryString, string $companyId): ReviewCollectionResponse;

    public function createCompanyReview(CreateCompanyReviewRequestDto $requestDto): void;

    public function getProductReviews(ProductReviewQueryString $queryString, string $productId): ReviewCollectionResponse;

    public function createProductReview(CreateProductReviewRequestDto $requestDto): void;

    public function createListProductReview(CreateListProductReviewRequestDto $requestDto): void;

    public function edit(EditReviewWithReviewerTokenRequest $request, int $reviewId): void;

    public function remove(RemoveReviewRequestDto $requestDto, int $reviewId): void;
}
