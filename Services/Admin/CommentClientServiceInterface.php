<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Admin;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;

interface CommentClientServiceInterface
{
    public function getRemovedComments(CommentQueryString $queryString): CommentCollectionResponse;

    public function edit(EditCommentRequestDto $requestDto, int $commentId): void;

    public function remove(int $commentId): void;

    public function read(int $commentId): void;
}
