<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Admin;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class CommentClientService implements CommentClientServiceInterface
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private NormalizerInterface $normalizer;
    private string $token;

    public function __construct(
        HttpClientInterface $client,
        UserAuthenticatorInterface $authenticator,
        SerializerInterface $serializer,
        NormalizerInterface $normalizer
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->normalizer = $normalizer;
        $this->token = $authenticator->auth();
    }

    public function getRemovedComments(CommentQueryString $queryString): CommentCollectionResponse
    {
        $response = $this->client->request(
            Request::METHOD_GET,
            '/api/admin/removed-comments',
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'query' => $this->normalizer->normalize($queryString),
            ]
        );

        return $this->serializer->deserialize(
            $response->getContent(),
            CommentCollectionResponse::class,
            SerializerConfig::JSON
        );
    }

    public function edit(EditCommentRequestDto $requestDto, int $commentId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/admin/comment/%d', $commentId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $requestDto,
            ]
        )->getContent();
    }

    public function remove(int $commentId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/admin/comment/%d/remove', $commentId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
            ]
        )->getContent();
    }

    public function read(int $commentId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/admin/comment/%d/read', $commentId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
            ]
        )->getContent();
    }
}
