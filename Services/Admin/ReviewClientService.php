<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Admin;

use Slivki\Bundle\ReviewBundle\Request\EditReviewRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\BaseReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class ReviewClientService implements ReviewClientServiceInterface
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private NormalizerInterface $normalizer;
    private string $token;

    public function __construct(
        HttpClientInterface $client,
        UserAuthenticatorInterface $authenticator,
        SerializerInterface $serializer,
        NormalizerInterface $normalizer
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->normalizer = $normalizer;
        $this->token = $authenticator->auth();
    }

    public function getNewReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse
    {
        $response = $this->client->request(
            Request::METHOD_GET,
            '/api/admin/reviews', [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'query' => $this->normalizer->normalize($queryString),
            ]
        );

        return $this->serializer->deserialize(
            $response->getContent(),
            ReviewCollectionResponse::class,
            SerializerConfig::JSON
        );
    }

    public function getRemovedReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse
    {
        $response = $this->client->request(
            Request::METHOD_GET,
            '/api/admin/removed-reviews',
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'query' => $this->normalizer->normalize($queryString),
            ]
        );

        return $this->serializer->deserialize(
            $response->getContent(),
            ReviewCollectionResponse::class,
            SerializerConfig::JSON
        );
    }

    public function edit(EditReviewRequest $request, int $reviewId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/admin/review/%d', $reviewId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $this->normalizer->normalize($request),
            ]
        )->getContent();
    }

    public function remove(int $reviewId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/admin/review/%d/remove', $reviewId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
            ]
        )->getContent();
    }

    public function read(int $reviewId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/admin/review/%d/read', $reviewId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
            ]
        )->getContent();
    }
}
