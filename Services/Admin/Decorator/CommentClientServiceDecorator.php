<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Admin\Decorator;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\Admin\CommentClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

final class CommentClientServiceDecorator implements CommentClientServiceInterface
{
    private CommentClientServiceInterface $commentClientService;

    public function __construct(CommentClientServiceInterface $commentClientService)
    {
        $this->commentClientService = $commentClientService;
    }

    public function getRemovedComments(CommentQueryString $queryString): CommentCollectionResponse
    {
        try {
            return $this->commentClientService->getRemovedComments($queryString);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function edit(EditCommentRequestDto $requestDto, int $commentId): void
    {
        try {
            $this->commentClientService->edit($requestDto, $commentId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function remove(int $commentId): void
    {
        try {
            $this->commentClientService->remove($commentId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function read(int $commentId): void
    {
        try {
            $this->commentClientService->read($commentId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }
}
