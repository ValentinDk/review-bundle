<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Admin\Decorator;

use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\EditReviewRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\BaseReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\Admin\ReviewClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

final class ReviewClientServiceDecorator implements ReviewClientServiceInterface
{
    private ReviewClientServiceInterface $reviewClientService;

    public function __construct(ReviewClientServiceInterface $reviewClientService)
    {
        $this->reviewClientService = $reviewClientService;
    }

    public function getNewReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse
    {
        try {
            return $this->reviewClientService->getNewReviews($queryString);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function getRemovedReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse
    {
        try {
            return $this->reviewClientService->getRemovedReviews($queryString);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function edit(EditReviewRequest $request, int $reviewId): void
    {
        try {
            $this->reviewClientService->edit($request, $reviewId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function remove(int $reviewId): void
    {
        try {
            $this->reviewClientService->remove($reviewId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function read(int $reviewId): void
    {
        try {
            $this->reviewClientService->read($reviewId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }
}
