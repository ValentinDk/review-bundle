<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Admin;

use Slivki\Bundle\ReviewBundle\Request\EditReviewRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\BaseReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;

interface ReviewClientServiceInterface
{
    public function getNewReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse;

    public function getRemovedReviews(BaseReviewQueryString $queryString): ReviewCollectionResponse;

    public function edit(EditReviewRequest $request, int $reviewId): void;

    public function remove(int $reviewId): void;

    public function read(int $reviewId): void;
}
