<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\CreateCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\RemoveCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;

interface CommentClientServiceInterface
{
    public function getComments(CommentQueryString $queryString, int $reviewId): CommentCollectionResponse;

    public function create(CreateCommentRequestDto $requestDto): void;

    public function edit(EditCommentRequestDto $requestDto, int $commentId): void;

    public function remove(RemoveCommentRequestDto $requestDto, int $commentId): void;
}
