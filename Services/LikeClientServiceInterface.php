<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;

interface LikeClientServiceInterface
{
    public function create(LikeRequestDto $requestDto): void;

    public function delete(LikeRequestDto $requestDto): void;
}
