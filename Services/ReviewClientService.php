<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateCompanyReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateListProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\RemoveReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Request\EditReviewWithReviewerTokenRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\CompanyReviewQueryString;
use Slivki\Bundle\ReviewBundle\Request\Query\ProductReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class ReviewClientService implements ReviewClientServiceInterface
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private NormalizerInterface $normalizer;
    private string $token;

    public function __construct(
        HttpClientInterface $client,
        UserAuthenticatorInterface $authenticator,
        SerializerInterface $serializer,
        NormalizerInterface $normalizer
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->normalizer = $normalizer;
        $this->token = $authenticator->auth();
    }

    public function getCompanyReviews(
        CompanyReviewQueryString $queryString,
        string $companyId
    ): ReviewCollectionResponse {
        $response = $this->client->request(
            Request::METHOD_GET,
            \sprintf('/api/company/%s/reviews', $companyId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'query' => $this->normalizer->normalize($queryString),
            ]
        );

        return $this->serializer->deserialize(
            $response->getContent(),
            ReviewCollectionResponse::class,
            SerializerConfig::JSON
        );
    }

    public function createCompanyReview(CreateCompanyReviewRequestDto $requestDto): void
    {
        $this->client->request(
            Request::METHOD_POST,
            '/api/company/review',
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $requestDto,
            ]
        )->getContent();
    }

    public function getProductReviews(
        ProductReviewQueryString $queryString,
        string $productId
    ): ReviewCollectionResponse {
        $response = $this->client->request(
            Request::METHOD_GET,
            \sprintf('/api/product/%s/reviews', $productId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'query' => $this->normalizer->normalize($queryString),
            ]
        );

        return $this->serializer->deserialize(
            $response->getContent(),
            ReviewCollectionResponse::class,
            SerializerConfig::JSON
        );
    }

    public function createProductReview(CreateProductReviewRequestDto $requestDto): void
    {
        $this->client->request(
            Request::METHOD_POST,
            '/api/product/review',
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $requestDto,
            ]
        )->getContent();
    }

    public function createListProductReview(CreateListProductReviewRequestDto $requestDto): void
    {
        $this->client->request(
            Request::METHOD_POST,
            '/api/products/review',
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $requestDto,
            ]
        )->getContent();
    }

    public function edit(EditReviewWithReviewerTokenRequest $request, int $reviewId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/review/%d', $reviewId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $this->normalizer->normalize($request),
            ]
        )->getContent();
    }

    public function remove(RemoveReviewRequestDto $requestDto, int $reviewId): void
    {
        $this->client->request(
            Request::METHOD_PATCH,
            \sprintf('/api/review/%d/remove', $reviewId),
            [
                'headers' => ['Accept' => 'application/json'],
                'auth_bearer' => $this->token,
                'verify' => false,
                'json' => $requestDto,
            ]
        )->getContent();
    }
}
