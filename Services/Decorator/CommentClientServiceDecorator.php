<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Decorator;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\CreateCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\RemoveCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\CommentClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

final class CommentClientServiceDecorator implements CommentClientServiceInterface
{
    private CommentClientServiceInterface $commentClientService;

    public function __construct(CommentClientServiceInterface $commentClientService)
    {
        $this->commentClientService = $commentClientService;
    }

    public function getComments(CommentQueryString $queryString, int $reviewId): CommentCollectionResponse
    {
        try {
            return $this->commentClientService->getComments($queryString, $reviewId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function create(CreateCommentRequestDto $requestDto): void
    {
        try {
            $this->commentClientService->create($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function edit(EditCommentRequestDto $requestDto, int $commentId): void
    {
        try {
            $this->commentClientService->edit($requestDto, $commentId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function remove(RemoveCommentRequestDto $requestDto, int $commentId): void
    {
        try {
            $this->commentClientService->remove($requestDto, $commentId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }
}
