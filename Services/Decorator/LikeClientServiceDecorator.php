<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Decorator;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Services\LikeClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

final class LikeClientServiceDecorator implements LikeClientServiceInterface
{
    private LikeClientServiceInterface $likeClientService;

    public function __construct(LikeClientServiceInterface $likeClientService)
    {
        $this->likeClientService = $likeClientService;
    }

    public function create(LikeRequestDto $requestDto): void
    {
        try {
            $this->likeClientService->create($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function delete(LikeRequestDto $requestDto): void
    {
        try {
            $this->likeClientService->delete($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }
}
