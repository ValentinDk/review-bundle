<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Decorator;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Services\DislikeClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

final class DislikeClientServiceDecorator implements DislikeClientServiceInterface
{
    private DislikeClientServiceInterface $dislikeClientService;

    public function __construct(DislikeClientServiceInterface $dislikeClientService)
    {
        $this->dislikeClientService = $dislikeClientService;
    }

    public function create(LikeRequestDto $requestDto): void
    {
        try {
            $this->dislikeClientService->create($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function delete(LikeRequestDto $requestDto): void
    {
        try {
            $this->dislikeClientService->delete($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }
}
