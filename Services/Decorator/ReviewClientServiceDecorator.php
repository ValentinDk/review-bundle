<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Services\Decorator;

use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateCompanyReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateListProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\RemoveReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\EditReviewWithReviewerTokenRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\CompanyReviewQueryString;
use Slivki\Bundle\ReviewBundle\Request\Query\ProductReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\ReviewClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

final class ReviewClientServiceDecorator implements ReviewClientServiceInterface
{
    private ReviewClientServiceInterface $reviewClientService;

    public function __construct(ReviewClientServiceInterface $reviewClientService)
    {
        $this->reviewClientService = $reviewClientService;
    }

    public function getCompanyReviews(
        CompanyReviewQueryString $queryString,
        string $companyId
    ): ReviewCollectionResponse {
        try {
            return $this->reviewClientService->getCompanyReviews($queryString, $companyId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function createCompanyReview(CreateCompanyReviewRequestDto $requestDto): void
    {
        try {
            $this->reviewClientService->createCompanyReview($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function getProductReviews(
        ProductReviewQueryString $queryString,
        string $productId
    ): ReviewCollectionResponse {
        try {
            return $this->reviewClientService->getProductReviews($queryString, $productId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function createProductReview(CreateProductReviewRequestDto $requestDto): void
    {
        try {
            $this->reviewClientService->createProductReview($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function createListProductReview(CreateListProductReviewRequestDto $requestDto): void
    {
        try {
            $this->reviewClientService->createListProductReview($requestDto);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function edit(EditReviewWithReviewerTokenRequest $request, int $reviewId): void
    {
        try {
            $this->reviewClientService->edit($request, $reviewId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }

    public function remove(RemoveReviewRequestDto $requestDto, int $reviewId): void
    {
        try {
            $this->reviewClientService->remove($requestDto, $reviewId);
        } catch (ClientExceptionInterface $e) {
            throw new ClientException($e->getResponse()->getContent(false), $e->getCode(), $e);
        }
    }
}
