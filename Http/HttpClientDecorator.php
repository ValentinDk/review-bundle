<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Http;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;

final class HttpClientDecorator implements HttpClientInterface
{
    private HttpClientInterface $client;
    private string $baseUri;

    public function __construct(HttpClientInterface $client, string $baseUri)
    {
        $this->client = $client;
        $this->baseUri = $baseUri;
    }

    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        if (!isset($options['base_uri'])) {
            $options['base_uri'] = $this->baseUri;
        }

        return $this->client->request($method, $url, $options);
    }

    public function stream($responses, float $timeout = null): ResponseStreamInterface
    {
        return $this->client->stream($responses, $timeout);
    }
}
