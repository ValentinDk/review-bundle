<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Http;

final class ClientException extends \Exception
{
}
