<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Product\ProductRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateCompanyReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateListProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\RemoveReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Request\EditReviewWithReviewerTokenRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\CompanyReviewQueryString;
use Slivki\Bundle\ReviewBundle\Request\Query\ProductReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Slivki\Bundle\ReviewBundle\Services\ReviewClientService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class ReviewClientServiceTest extends TestCase
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private NormalizerInterface $normalizer;
    private ReviewClientService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(HttpClientInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->normalizer = $this->createMock(NormalizerInterface::class);
        $this->service = new ReviewClientService(
            $this->client,
            $this->createConfiguredMock(UserAuthenticatorInterface::class, ['auth' => 'testToken']),
            $this->serializer,
            $this->normalizer,
        );
    }

    public function testGetCompanyReviews(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_GET, $method);
                self::assertSame('/api/company/testId/reviews', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertSame([], $options['query']);

                return $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']);
            });
        $this->normalizer->expects(self::once())->method('normalize')
            ->willReturnCallback(static function (object $object): array {
                self::assertInstanceOf(CompanyReviewQueryString::class, $object);

                return [];
            });
        $this->serializer->expects(self::once())->method('deserialize')
            ->willReturnCallback(static function (string $data, string $type, string $format): ReviewCollectionResponse {
                self::assertSame(ReviewCollectionResponse::class, $type);
                self::assertSame(SerializerConfig::JSON, $format);

                return new ReviewCollectionResponse();
            });


        $this->service->getCompanyReviews(new CompanyReviewQueryString(), 'testId');
    }

    public function testGetProductReviews(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_GET, $method);
                self::assertSame('/api/product/testId/reviews', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertSame([], $options['query']);

                return $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']);
            });
        $this->normalizer->expects(self::once())->method('normalize')
            ->willReturnCallback(static function (object $object): array {
                self::assertInstanceOf(ProductReviewQueryString::class, $object);

                return [];
            });
        $this->serializer->expects(self::once())->method('deserialize')
            ->willReturnCallback(static function (string $data, string $type, string $format): ReviewCollectionResponse {
                self::assertSame(ReviewCollectionResponse::class, $type);
                self::assertSame(SerializerConfig::JSON, $format);

                return new ReviewCollectionResponse();
            });

        $this->service->getProductReviews(new ProductReviewQueryString(), 'testId');
    }

    public function testCreateCompanyReview(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_POST, $method);
                self::assertSame('/api/company/review', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertInstanceOf(CreateCompanyReviewRequestDto::class, $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->createCompanyReview(
            new CreateCompanyReviewRequestDto(
                new CreateReviewRequestDto('testText', 5, []),
                new CompanyRequestDto('testCompanyId', 'testCompanyName'),
                'testToken'
            )
        );
    }

    public function testCreateProductReview(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_POST, $method);
                self::assertSame('/api/product/review', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertInstanceOf(CreateProductReviewRequestDto::class, $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->createProductReview(
            new CreateProductReviewRequestDto(
                new CreateReviewRequestDto('testText', 5, []),
                new ProductRequestDto(
                    new CompanyRequestDto('testCompanyId', 'testCompanyName'),
                    'testProductId',
                    'testProductName'
                ),
                'testToken'
            )
        );
    }

    public function testCreateList(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_POST, $method);
                self::assertSame('/api/products/review', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertInstanceOf(CreateListProductReviewRequestDto::class, $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->createListProductReview(
            new CreateListProductReviewRequestDto(
                new CreateReviewRequestDto('', 5, []),
                [new ProductRequestDto(new CompanyRequestDto('', ''), '', '')],
                'testToken',
            ),
        );
    }

    public function testEdit(): void
    {
        $this->normalizer->expects(self::once())->method('normalize')
            ->willReturnCallback(static function (object $object): array {
                self::assertInstanceOf(EditReviewWithReviewerTokenRequest::class, $object);

                return ['reviewerToken' => 'testToken'];
            });
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/review/1', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertSame(['reviewerToken' => 'testToken'], $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->edit(new EditReviewWithReviewerTokenRequest('testToken'), 1);
    }

    public function testRemove(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/review/1/remove', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertInstanceOf(RemoveReviewRequestDto::class, $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->remove(new RemoveReviewRequestDto('testToken'), 1);
    }
}
