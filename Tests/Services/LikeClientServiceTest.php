<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Slivki\Bundle\ReviewBundle\Services\LikeClientService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class LikeClientServiceTest extends TestCase
{
    private HttpClientInterface $client;
    private LikeClientService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(HttpClientInterface::class);
        $this->service = new LikeClientService(
            $this->client,
            $this->createConfiguredMock(UserAuthenticatorInterface::class, ['auth' => 'testToken'])
        );
    }

    public function testCreate(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_POST, $method);
                self::assertSame('/api/like', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertInstanceOf(LikeRequestDto::class, $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->create(new LikeRequestDto(1, 'testToken'));
    }

    public function testDelete(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_DELETE, $method);
                self::assertSame('/api/like', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertInstanceOf(LikeRequestDto::class, $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->delete(new LikeRequestDto(1, 'testToken'));
    }
}
