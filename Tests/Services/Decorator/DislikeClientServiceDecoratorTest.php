<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services\Decorator;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Services\Decorator\DislikeClientServiceDecorator;
use Slivki\Bundle\ReviewBundle\Services\DislikeClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class DislikeClientServiceDecoratorTest extends TestCase
{
    private DislikeClientServiceInterface $service;
    private DislikeClientServiceDecorator $decorator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->createMock(DislikeClientServiceInterface::class);
        $this->decorator = new DislikeClientServiceDecorator($this->service);
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorSuccess(LikeRequestDto $requestDto, string $methodName): void
    {
        $this->service->expects(self::once())->method($methodName);
        $this->decorator->$methodName($requestDto);
        self::assertTrue(true);
    }

    public function argumentAndMethodNameProvider(): \Generator
    {
        $requestDto = new LikeRequestDto(1, '');

        yield [$requestDto, 'create'];
        yield [$requestDto, 'delete'];
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorFailure(LikeRequestDto $requestDto, string $methodName): void
    {
        $exception = $this->createConfiguredMock(ClientExceptionInterface::class, [
            'getResponse' => $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']),
        ]);

        $this->service->expects(self::once())->method($methodName)->willThrowException($exception);

        $this->expectException(ClientException::class);
        $this->decorator->$methodName($requestDto);
    }
}
