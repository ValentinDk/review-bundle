<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services\Decorator;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Product\ProductRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateCompanyReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateListProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\RemoveReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\EditReviewWithReviewerTokenRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\CompanyReviewQueryString;
use Slivki\Bundle\ReviewBundle\Request\Query\ProductReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\Decorator\ReviewClientServiceDecorator;
use Slivki\Bundle\ReviewBundle\Services\ReviewClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class ReviewClientServiceDecoratorTest extends TestCase
{
    private ReviewClientServiceInterface $service;
    private ReviewClientServiceDecorator $decorator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->createMock(ReviewClientServiceInterface::class);
        $this->decorator = new ReviewClientServiceDecorator($this->service);
    }

    public function testGetCompanyReviewsSuccess(): void
    {
        $this->service->expects(self::once())->method('getCompanyReviews')->willReturn(new ReviewCollectionResponse());
        $this->decorator->getCompanyReviews(new CompanyReviewQueryString(), 'testId');
        self::assertTrue(true);
    }

    public function testGetCompanyReviewsFailure(): void
    {
        $this->service->expects(self::once())->method('getCompanyReviews')
            ->willThrowException($this->createException());

        $this->expectException(ClientException::class);
        $this->decorator->getCompanyReviews(new CompanyReviewQueryString(), 'testId');
    }

    public function testGetProductReviewsSuccess(): void
    {
        $this->service->expects(self::once())->method('getProductReviews')->willReturn(new ReviewCollectionResponse());
        $this->decorator->getProductReviews(new ProductReviewQueryString(), 'testId');
        self::assertTrue(true);
    }

    public function testGetProductReviewsFailure(): void
    {
        $this->service->expects(self::once())->method('getProductReviews')
            ->willThrowException($this->createException());

        $this->expectException(ClientException::class);
        $this->decorator->getProductReviews(new ProductReviewQueryString(), 'testId');
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorSuccess(array $arguments, string $methodName): void
    {
        $this->service->expects(self::once())->method($methodName);
        $this->decorator->$methodName(...$arguments);
        self::assertTrue(true);
    }

    public function argumentAndMethodNameProvider(): \Generator
    {
        $reviewRequest = new CreateReviewRequestDto('', 5, []);
        $companyRequest = new CompanyRequestDto('', '');
        $productRequest = new ProductRequestDto($companyRequest, '', '');

        yield [[new CreateCompanyReviewRequestDto($reviewRequest, $companyRequest, '')], 'createCompanyReview'];
        yield [[new CreateProductReviewRequestDto($reviewRequest, $productRequest, '')], 'createProductReview'];
        yield [[new CreateListProductReviewRequestDto($reviewRequest, [$productRequest], '')], 'createListProductReview'];
        yield [[new EditReviewWithReviewerTokenRequest(''), 1], 'edit'];
        yield [[new RemoveReviewRequestDto(''), 1], 'remove'];
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorFailure(array $arguments, string $methodName): void
    {
        $this->service->expects(self::once())->method($methodName)
            ->willThrowException($this->createException());

        $this->expectException(ClientException::class);
        $this->decorator->$methodName(...$arguments);
    }

    private function createException(): ClientExceptionInterface
    {
        return $this->createConfiguredMock(ClientExceptionInterface::class, [
            'getResponse' => $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']),
        ]);
    }
}
