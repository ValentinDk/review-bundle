<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services\Decorator;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\CreateCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\RemoveCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\CommentClientServiceInterface;
use Slivki\Bundle\ReviewBundle\Services\Decorator\CommentClientServiceDecorator;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class CommentClientServiceDecoratorTest extends TestCase
{
    private CommentClientServiceInterface $service;
    private CommentClientServiceDecorator $decorator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->createMock(CommentClientServiceInterface::class);
        $this->decorator = new CommentClientServiceDecorator($this->service);
    }

    public function testGetCommentsSuccess(): void
    {
        $this->service->expects(self::once())->method('getComments')->willReturn(new CommentCollectionResponse());
        $this->decorator->getComments(new CommentQueryString(), 1);
        self::assertTrue(true);
    }

    public function testGetCommentsFailure(): void
    {
        $exception = $this->createConfiguredMock(ClientExceptionInterface::class, [
            'getResponse' => $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']),
        ]);

        $this->service->expects(self::once())->method('getComments')->willThrowException($exception);

        $this->expectException(ClientException::class);
        $this->decorator->getComments(new CommentQueryString(), 1);
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorSuccess(array $arguments, string $methodName): void
    {
        $this->service->expects(self::once())->method($methodName);
        $this->decorator->$methodName(...$arguments);
        self::assertTrue(true);
    }

    public function argumentAndMethodNameProvider(): \Generator
    {
        yield [[new CreateCommentRequestDto(1, '', null, '')], 'create'];
        yield [[new EditCommentRequestDto('', ''), 1], 'edit'];
        yield [[new RemoveCommentRequestDto(''), 1], 'remove'];
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorFailure(array $arguments, string $methodName): void
    {
        $exception = $this->createConfiguredMock(ClientExceptionInterface::class, [
            'getResponse' => $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']),
        ]);

        $this->service->expects(self::once())->method($methodName)->willThrowException($exception);

        $this->expectException(ClientException::class);
        $this->decorator->$methodName(...$arguments);
    }
}
