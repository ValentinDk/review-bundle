<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services\Admin;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Slivki\Bundle\ReviewBundle\Services\Admin\CommentClientService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class CommentClientServiceTest extends TestCase
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private NormalizerInterface $normalizer;
    private CommentClientService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(HttpClientInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->normalizer = $this->createMock(NormalizerInterface::class);
        $this->service = new CommentClientService(
            $this->client,
            $this->createConfiguredMock(UserAuthenticatorInterface::class, ['auth' => 'testToken']),
            $this->serializer,
            $this->normalizer,
        );
    }

    public function testGetRemovedComments(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_GET, $method);
                self::assertSame('/api/admin/removed-comments', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertSame([], $options['query']);

                return $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']);
            });
        $this->normalizer->expects(self::once())->method('normalize')
            ->willReturnCallback(static function (object $object): array {
                self::assertInstanceOf(CommentQueryString::class, $object);

                return [];
            });
        $this->serializer->expects(self::once())->method('deserialize')
            ->willReturnCallback(static function (string $data, string $type, string $format): CommentCollectionResponse {
                self::assertSame(CommentCollectionResponse::class, $type);
                self::assertSame(SerializerConfig::JSON, $format);

                return new CommentCollectionResponse();
            });

        $this->service->getRemovedComments(new CommentQueryString());
    }

    public function testEdit(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/admin/comment/1', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertInstanceOf(EditCommentRequestDto::class, $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->edit(new EditCommentRequestDto(''), 1);
    }

    public function testRemove(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/admin/comment/1/remove', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->remove(1);
    }

    public function testRead(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/admin/comment/1/read', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->read(1);
    }
}
