<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services\Admin\Decorator;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin\EditCommentRequestDto;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\Admin\CommentClientServiceInterface;
use Slivki\Bundle\ReviewBundle\Services\Admin\Decorator\CommentClientServiceDecorator;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class CommentClientServiceDecoratorTest extends TestCase
{
    private CommentClientServiceInterface $service;
    private CommentClientServiceDecorator $decorator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->createMock(CommentClientServiceInterface::class);
        $this->decorator = new CommentClientServiceDecorator($this->service);
    }

    public function testGetRemovedCommentsSuccess(): void
    {
        $this->service->expects(self::once())->method('getRemovedComments')->willReturn(new CommentCollectionResponse());
        $this->decorator->getRemovedComments(new CommentQueryString());
        self::assertTrue(true);
    }

    public function testGetRemovedCommentsFailure(): void
    {
        $exception = $this->createConfiguredMock(ClientExceptionInterface::class, [
            'getResponse' => $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']),
        ]);

        $this->service->expects(self::once())->method('getRemovedComments')->willThrowException($exception);

        $this->expectException(ClientException::class);
        $this->decorator->getRemovedComments(new CommentQueryString());
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorSuccess(array $arguments, string $methodName): void
    {
        $this->service->expects(self::once())->method($methodName);
        $this->decorator->$methodName(...$arguments);
        self::assertTrue(true);
    }

    public function argumentAndMethodNameProvider(): \Generator
    {
        yield [[new EditCommentRequestDto(''), 1], 'edit'];
        yield [[1], 'remove'];
        yield [[1], 'read'];
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorFailure(array $arguments, string $methodName): void
    {
        $exception = $this->createConfiguredMock(ClientExceptionInterface::class, [
            'getResponse' => $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']),
        ]);

        $this->service->expects(self::once())->method($methodName)->willThrowException($exception);

        $this->expectException(ClientException::class);
        $this->decorator->$methodName(...$arguments);
    }
}
