<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services\Admin\Decorator;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Http\ClientException;
use Slivki\Bundle\ReviewBundle\Request\EditReviewRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\BaseReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Services\Admin\Decorator\ReviewClientServiceDecorator;
use Slivki\Bundle\ReviewBundle\Services\Admin\ReviewClientServiceInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class ReviewClientServiceDecoratorTest extends TestCase
{
    private ReviewClientServiceInterface $service;
    private ReviewClientServiceDecorator $decorator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->createMock(ReviewClientServiceInterface::class);
        $this->decorator = new ReviewClientServiceDecorator($this->service);
    }

    public function testGetNewReviewsSuccess(): void
    {
        $this->service->expects(self::once())->method('getNewReviews')->willReturn(new ReviewCollectionResponse());
        $this->decorator->getNewReviews(new BaseReviewQueryString());
        self::assertTrue(true);
    }

    public function testGetNewReviewsFailure(): void
    {
        $this->service->expects(self::once())->method('getNewReviews')
            ->willThrowException($this->createException());

        $this->expectException(ClientException::class);
        $this->decorator->getNewReviews(new BaseReviewQueryString());
    }

    public function testGetRemovedReviewsSuccess(): void
    {
        $this->service->expects(self::once())->method('getRemovedReviews')->willReturn(new ReviewCollectionResponse());
        $this->decorator->getRemovedReviews(new BaseReviewQueryString());
        self::assertTrue(true);
    }

    public function testGetRemovedReviewsFailure(): void
    {
        $this->service->expects(self::once())->method('getRemovedReviews')
            ->willThrowException($this->createException());

        $this->expectException(ClientException::class);
        $this->decorator->getRemovedReviews(new BaseReviewQueryString());
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorSuccess(array $arguments, string $methodName): void
    {
        $this->service->expects(self::once())->method($methodName);
        $this->decorator->$methodName(...$arguments);
        self::assertTrue(true);
    }

    public function argumentAndMethodNameProvider(): \Generator
    {
        yield [[new EditReviewRequest(), 1], 'edit'];
        yield [[1], 'remove'];
        yield [[1], 'read'];
    }

    /**
     * @dataProvider argumentAndMethodNameProvider
     */
    public function testDecoratorFailure(array $arguments, string $methodName): void
    {
        $this->service->expects(self::once())->method($methodName)
            ->willThrowException($this->createException());

        $this->expectException(ClientException::class);
        $this->decorator->$methodName(...$arguments);
    }

    private function createException(): ClientExceptionInterface
    {
        return $this->createConfiguredMock(ClientExceptionInterface::class, [
            'getResponse' => $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']),
        ]);
    }
}
