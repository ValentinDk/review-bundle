<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Services\Admin;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Request\EditReviewRequest;
use Slivki\Bundle\ReviewBundle\Request\Query\BaseReviewQueryString;
use Slivki\Bundle\ReviewBundle\Response\Review\ReviewCollectionResponse;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticatorInterface;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Slivki\Bundle\ReviewBundle\Services\Admin\ReviewClientService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class ReviewClientServiceTest extends TestCase
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private NormalizerInterface $normalizer;
    private ReviewClientService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(HttpClientInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->normalizer = $this->createMock(NormalizerInterface::class);
        $this->service = new ReviewClientService(
            $this->client,
            $this->createConfiguredMock(UserAuthenticatorInterface::class, ['auth' => 'testToken']),
            $this->serializer,
            $this->normalizer,
        );
    }

    public function testGetNewReviews(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_GET, $method);
                self::assertSame('/api/admin/reviews', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertSame([], $options['query']);

                return $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']);
            });
        $this->normalizer->expects(self::once())->method('normalize')
            ->willReturnCallback(static function (object $object): array {
                self::assertInstanceOf(BaseReviewQueryString::class, $object);

                return [];
            });
        $this->serializer->expects(self::once())->method('deserialize')
            ->willReturnCallback(static function (string $data, string $type, string $format): ReviewCollectionResponse {
                self::assertSame(ReviewCollectionResponse::class, $type);
                self::assertSame(SerializerConfig::JSON, $format);

                return new ReviewCollectionResponse();
            });

        $this->service->getNewReviews(new BaseReviewQueryString());
    }

    public function testGetRemovedReviews(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_GET, $method);
                self::assertSame('/api/admin/removed-reviews', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertSame([], $options['query']);

                return $this->createConfiguredMock(ResponseInterface::class, ['getContent' => '']);
            });
        $this->normalizer->expects(self::once())->method('normalize')
            ->willReturnCallback(static function (object $object): array {
                self::assertInstanceOf(BaseReviewQueryString::class, $object);

                return [];
            });
        $this->serializer->expects(self::once())->method('deserialize')
            ->willReturnCallback(static function (string $data, string $type, string $format): ReviewCollectionResponse {
                self::assertSame(ReviewCollectionResponse::class, $type);
                self::assertSame(SerializerConfig::JSON, $format);

                return new ReviewCollectionResponse();
            });

        $this->service->getRemovedReviews(new BaseReviewQueryString());
    }

    public function testEdit(): void
    {
        $this->normalizer->expects(self::once())->method('normalize')
            ->willReturnCallback(static function (object $object): array {
                self::assertInstanceOf(EditReviewRequest::class, $object);

                return ['name' => 'testEditName'];
            });
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/admin/review/1', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);
                self::assertSame(['name' => 'testEditName'], $options['json']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->edit(new EditReviewRequest(), 1);
    }

    public function testRemove(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/admin/review/1/remove', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->remove(1);
    }

    public function testRead(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $options): ResponseInterface {
                self::assertSame(Request::METHOD_PATCH, $method);
                self::assertSame('/api/admin/review/1/read', $url);
                self::assertSame(['Accept' => 'application/json'], $options['headers']);
                self::assertSame('testToken', $options['auth_bearer']);
                self::assertFalse($options['verify']);

                return $this->createMock(ResponseInterface::class);
            });

        $this->service->read(1);
    }
}
