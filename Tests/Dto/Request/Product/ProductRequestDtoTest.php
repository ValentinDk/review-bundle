<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Product;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Product\ProductRequestDto;

final class ProductRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $companyRequest = new CompanyRequestDto('', '');
        $productRequest = (
            new ProductRequestDto($companyRequest, 'testProductId', 'testProductName')
        )->jsonSerialize();

        self::assertInstanceOf(CompanyRequestDto::class, $productRequest['company']);
        self::assertSame('testProductId', $productRequest['id']);
        self::assertSame('testProductName', $productRequest['name']);
    }
}
