<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Company;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;

final class CompanyRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        self::assertSame(
            ['id' => 'testId', 'name' => 'testName'],
            (new CompanyRequestDto('testId', 'testName'))->jsonSerialize()
        );
    }
}
