<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateCompanyReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateReviewRequestDto;

final class CreateCompanyReviewRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $reviewRequest = new CreateReviewRequestDto('', 0, []);
        $companyRequest = new CompanyRequestDto('', '');
        $request = (
            new CreateCompanyReviewRequestDto($reviewRequest, $companyRequest, 'testToken')
        )->jsonSerialize();

        self::assertInstanceOf(CompanyRequestDto::class, $request['company']);
        self::assertInstanceOf(CreateReviewRequestDto::class, $request['review']);
        self::assertSame('testToken', $request['reviewerToken']);
    }
}
