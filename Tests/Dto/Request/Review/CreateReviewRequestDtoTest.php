<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateReviewRequestDto;

final class CreateReviewRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $request = new CreateReviewRequestDto('testText', 5, ['http://test.test']);
        $expected = [
            'text' => 'testText',
            'rating' => 5,
            'imageUrls' => ['http://test.test'],
        ];

        self::assertSame($expected, $request->jsonSerialize());
    }
}
