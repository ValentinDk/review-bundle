<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review\Like;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like\LikeRequestDto;

final class LikeRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        self::assertSame(
            ['reviewId' => 1, 'reviewerToken' => 'testToken'],
            (new LikeRequestDto(1, 'testToken'))->jsonSerialize()
        );
    }
}
