<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Product\ProductRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateListProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateReviewRequestDto;

final class CreateListProductReviewRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $reviewRequest = new CreateReviewRequestDto('', 0, []);
        $productRequest = new ProductRequestDto(new CompanyRequestDto('', ''), '', '');
        $request = (
            new CreateListProductReviewRequestDto(
                $reviewRequest,
                [$productRequest, $productRequest],
                'testToken'
            )
        )->jsonSerialize();

        foreach ($request['products'] as $product) {
            self::assertInstanceOf(ProductRequestDto::class, $product);
        }

        self::assertCount(2, $request['products']);
        self::assertInstanceOf(CreateReviewRequestDto::class, $request['review']);
        self::assertSame('testToken', $request['reviewerToken']);
    }
}
