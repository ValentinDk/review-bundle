<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\RemoveReviewRequestDto;

final class RemoveReviewRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        self::assertSame(
            ['reviewerToken' => 'testToken'],
            (new RemoveReviewRequestDto('testToken'))->jsonSerialize()
        );
    }
}
