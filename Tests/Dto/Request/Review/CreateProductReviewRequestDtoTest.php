<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Product\ProductRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateProductReviewRequestDto;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\CreateReviewRequestDto;

final class CreateProductReviewRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $reviewRequest = new CreateReviewRequestDto('', 0, []);
        $companyRequest = new ProductRequestDto(new CompanyRequestDto('', ''), '', '');
        $request = (
            new CreateProductReviewRequestDto($reviewRequest, $companyRequest, 'testToken')
        )->jsonSerialize();

        self::assertInstanceOf(ProductRequestDto::class, $request['product']);
        self::assertInstanceOf(CreateReviewRequestDto::class, $request['review']);
        self::assertSame('testToken', $request['reviewerToken']);
    }
}
