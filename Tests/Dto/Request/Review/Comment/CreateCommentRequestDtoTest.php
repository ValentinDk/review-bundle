<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review\Comment;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\CreateCommentRequestDto;

final class CreateCommentRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $request = new CreateCommentRequestDto(1, 'testToken', null, 'testText');
        $expected = [
            'reviewId' => 1,
            'reviewerToken' => 'testToken',
            'parentId' => null,
            'text' => 'testText',
        ];

        self::assertSame($expected, $request->jsonSerialize());
    }
}
