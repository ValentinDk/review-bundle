<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review\Comment\Admin;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin\EditCommentRequestDto;

final class EditCommentRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        self::assertSame(['text' => 'testText'], (new EditCommentRequestDto('testText'))->jsonSerialize());
    }
}
