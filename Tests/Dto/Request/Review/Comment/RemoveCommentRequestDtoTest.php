<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review\Comment;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\RemoveCommentRequestDto;

final class RemoveCommentRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        self::assertSame(
            ['reviewerToken' => 'testToken'],
            (new RemoveCommentRequestDto('testToken'))->jsonSerialize()
        );
    }
}
