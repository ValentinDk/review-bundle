<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Dto\Request\Review\Comment;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\EditCommentRequestDto;

final class EditCommentRequestDtoTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $request = new EditCommentRequestDto('testToken', 'testText');
        $expected = [
            'reviewerToken' => 'testToken',
            'text' => 'testText',
        ];

        self::assertSame($expected, $request->jsonSerialize());
    }
}
