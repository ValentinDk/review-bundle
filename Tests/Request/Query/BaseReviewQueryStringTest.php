<?php

declare(strict_types=1);

namespace Request\Query;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Enum\BaseReviewSortType;
use Slivki\Bundle\ReviewBundle\Enum\SortDirectionType;
use Slivki\Bundle\ReviewBundle\Request\Query\BaseReviewQueryString;

final class BaseReviewQueryStringTest extends TestCase
{
    public function testGetDirection(): void
    {
        $request = new BaseReviewQueryString();
        $sortDirection = SortDirectionType::byValue(SortDirectionType::ASC);

        $request->direction = $sortDirection;

        self::assertSame($sortDirection->getValue(), $request->getDirection());
    }

    public function testGetSort(): void
    {
        $request = new BaseReviewQueryString();
        $sort = BaseReviewSortType::byValue(BaseReviewSortType::CREATED_AT);

        $request->sort = $sort;

        self::assertSame($sort->getValue(), $request->getSort());
    }
}
