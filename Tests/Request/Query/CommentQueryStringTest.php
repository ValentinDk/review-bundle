<?php

declare(strict_types=1);

namespace Request\Query;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Enum\CommentSortType;
use Slivki\Bundle\ReviewBundle\Enum\SortDirectionType;
use Slivki\Bundle\ReviewBundle\Request\Query\CommentQueryString;

final class CommentQueryStringTest extends TestCase
{
    public function testGetDirection(): void
    {
        $request = new CommentQueryString();
        $sortDirection = SortDirectionType::byValue(SortDirectionType::ASC);

        $request->direction = $sortDirection;

        self::assertSame($sortDirection->getValue(), $request->getDirection());
    }

    public function testGetSort(): void
    {
        $request = new CommentQueryString();
        $sort = CommentSortType::byValue(CommentSortType::CREATED_AT);

        $request->sort = $sort;

        self::assertSame($sort->getValue(), $request->getSort());
    }
}
