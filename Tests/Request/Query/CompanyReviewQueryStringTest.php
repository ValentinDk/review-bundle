<?php

declare(strict_types=1);

namespace Request\Query;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Enum\CompanyReviewSortType;
use Slivki\Bundle\ReviewBundle\Enum\SortDirectionType;
use Slivki\Bundle\ReviewBundle\Request\Query\CompanyReviewQueryString;

final class CompanyReviewQueryStringTest extends TestCase
{
    public function testGetDirection(): void
    {
        $request = new CompanyReviewQueryString();
        $sortDirection = SortDirectionType::byValue(SortDirectionType::ASC);

        $request->direction = $sortDirection;

        self::assertSame($sortDirection->getValue(), $request->getDirection());
    }

    public function testGetSort(): void
    {
        $request = new CompanyReviewQueryString();
        $sort = CompanyReviewSortType::byValue(CompanyReviewSortType::CREATED_AT);

        $request->sort = $sort;

        self::assertSame($sort->getValue(), $request->getSort());
    }
}
