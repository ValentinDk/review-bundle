<?php

declare(strict_types=1);

namespace Request\Query;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Enum\ProductReviewSortType;
use Slivki\Bundle\ReviewBundle\Enum\SortDirectionType;
use Slivki\Bundle\ReviewBundle\Request\Query\ProductReviewQueryString;

final class ProductReviewQueryStringTest extends TestCase
{
    public function testGetDirection(): void
    {
        $request = new ProductReviewQueryString();
        $sortDirection = SortDirectionType::byValue(SortDirectionType::ASC);

        $request->direction = $sortDirection;

        self::assertSame($sortDirection->getValue(), $request->getDirection());
    }

    public function testGetSort(): void
    {
        $request = new ProductReviewQueryString();
        $sort = ProductReviewSortType::byValue(ProductReviewSortType::CREATED_AT);

        $request->sort = $sort;

        self::assertSame($sort->getValue(), $request->getSort());
    }
}
