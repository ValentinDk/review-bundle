<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Security;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Response\TokenResponse;
use Slivki\Bundle\ReviewBundle\Security\UserAuthenticator;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class UserAuthenticatorTest extends TestCase
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private UserAuthenticator $authenticator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(HttpClientInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->authenticator = new UserAuthenticator(
            $this->client,
            $this->serializer,
            'testUsername',
            'testPassword',
        );
    }

    public function testAuth(): void
    {
        $options = [
            'verify' => false,
            'json' => ['username' => 'testUsername', 'password' => 'testPassword'],
        ];
        $content = '{"reviewerToken": "testToken"}';

        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function (string $method, string $url, array $optionsFromArg) use ($options, $content): ResponseInterface {
                self::assertSame(Request::METHOD_POST, $method);
                self::assertSame('/api/login_check', $url);
                self::assertSame($options, $optionsFromArg);

                return $this->createConfiguredMock(ResponseInterface::class, ['getContent' => $content]);
            });
        $this->serializer->expects(self::once())->method('deserialize')
            ->willReturnCallback(static function (string $data, string $type, string $format) use ($content): TokenResponse {
                self::assertSame($content, $data);
                self::assertSame(TokenResponse::class, $type);
                self::assertSame(SerializerConfig::JSON, $format);

                $response = new TokenResponse();
                $response->token = 'testToken';

                return $response;
            });

        self::assertSame('testToken', $this->authenticator->auth());
    }
}
