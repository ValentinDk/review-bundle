<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Tests\Http;

use PHPUnit\Framework\TestCase;
use Slivki\Bundle\ReviewBundle\Http\HttpClientDecorator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class HttpClientDecoratorTest extends TestCase
{
    private HttpClientInterface $client;
    private HttpClientDecorator $decorator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(HttpClientInterface::class);
        $this->decorator = new HttpClientDecorator($this->client, 'testBaseUri');
    }

    public function testRequest(): void
    {
        $this->client->expects(self::once())->method('request')
            ->willReturnCallback(function ($method, $url, $options): ResponseInterface {
                self::assertSame(Request::METHOD_GET, $method);
                self::assertSame('testUrl', $url);
                self::assertSame(['base_uri' => 'testBaseUri'], $options);

                return $this->createMock(ResponseInterface::class);
            });
        $this->decorator->request(Request::METHOD_GET, 'testUrl');
    }

    public function testStream(): void
    {
        $this->client->expects(self::once())->method('stream');
        $this->decorator->stream([$this->createMock(ResponseInterface::class)]);
    }
}
