<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Response;

final class TokenResponse
{
    public string $token;
}
