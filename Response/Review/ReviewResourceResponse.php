<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Response\Review;

use Slivki\Bundle\ReviewBundle\Response\Review\Comment\CommentResourceResponse;

final class ReviewResourceResponse
{
    public int $id;
    public string $reviewer;
    public string $text;
    public int $rating;
    public int $countLikes;
    public int $countDislikes;

    /** @var CommentResourceResponse[] */
    public array $comments;
}
