<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Response\Review;

final class ReviewCollectionResponse
{
    /** @var ReviewResourceResponse[] */
    public array $items;
    public int $totalCount;
}
