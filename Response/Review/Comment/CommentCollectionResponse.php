<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Response\Review\Comment;

final class CommentCollectionResponse
{
    /** @var CommentResourceResponse[] */
    public array $items;
    public int $totalCount;
}
