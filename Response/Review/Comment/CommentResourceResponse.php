<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Response\Review\Comment;

final class CommentResourceResponse
{
    public int $id;
    public string $reviewer;
    public string $text;

    /** @var ChildCommentResourceResponse[] */
    public array $childComments;
}
