<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Enum;

use MabeEnum\Enum;

final class SortDirectionType extends Enum
{
    public const ASC = 'ASC';
    public const DESC = 'DESC';
}
