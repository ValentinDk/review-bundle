<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Enum;

use MabeEnum\Enum;

final class BaseReviewSortType extends Enum
{
    public const CREATED_AT = 'br.createdAt';
}
