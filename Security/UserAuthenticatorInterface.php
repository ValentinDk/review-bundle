<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Security;

interface UserAuthenticatorInterface
{
    public function auth(): string;
}
