<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Security;

use Slivki\Bundle\ReviewBundle\Response\TokenResponse;
use Slivki\Bundle\ReviewBundle\Serializer\Config\SerializerConfig;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class UserAuthenticator implements UserAuthenticatorInterface
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;
    private string $username;
    private string $password;

    public function __construct(
        HttpClientInterface $client,
        SerializerInterface $serializer,
        string $username,
        string $password
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->username = $username;
        $this->password = $password;
    }

    public function auth(): string
    {
        $response = $this->client->request(
            Request::METHOD_POST,
            '/api/login_check',
            [
                'verify' => false,
                'json' => ['username' => $this->username, 'password' => $this->password],
            ]
        );

        /** @var TokenResponse $tokenResponse */
        $tokenResponse = $this->serializer->deserialize(
            $response->getContent(),
            TokenResponse::class,
            SerializerConfig::JSON
        );

        return $tokenResponse->token;
    }
}
