<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Request;

final class EditReviewWithReviewerTokenRequest
{
    public string $reviewerToken;
    public string $name;
    public int $rating;

    /** @var string[] */
    public array $imageUrls;

    public function __construct(string $reviewerToken)
    {
        $this->reviewerToken = $reviewerToken;
    }
}
