<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Request;

final class EditReviewRequest
{
    public string $name;
    public int $rating;

    /** @var string[] */
    public array $imageUrls;
}
