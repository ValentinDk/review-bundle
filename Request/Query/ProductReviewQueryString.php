<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Request\Query;

use Slivki\Bundle\ReviewBundle\Enum\ProductReviewSortType;
use Slivki\Bundle\ReviewBundle\Enum\SortDirectionType;

final class ProductReviewQueryString
{
    public int $page;
    public int $perPage;
    public SortDirectionType $direction;
    public ProductReviewSortType $sort;

    public function getDirection(): string
    {
        return $this->direction->getValue();
    }

    public function getSort(): string
    {
        return $this->sort->getValue();
    }
}
