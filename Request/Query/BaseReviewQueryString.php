<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Request\Query;

use Slivki\Bundle\ReviewBundle\Enum\BaseReviewSortType;
use Slivki\Bundle\ReviewBundle\Enum\SortDirectionType;

final class BaseReviewQueryString
{
    public int $page;
    public int $perPage;
    public SortDirectionType $direction;
    public BaseReviewSortType $sort;

    public function getDirection(): string
    {
        return $this->direction->getValue();
    }

    public function getSort(): string
    {
        return $this->sort->getValue();
    }
}
