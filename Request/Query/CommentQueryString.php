<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Request\Query;

use Slivki\Bundle\ReviewBundle\Enum\CommentSortType;
use Slivki\Bundle\ReviewBundle\Enum\SortDirectionType;

final class CommentQueryString
{
    public int $page;
    public int $perPage;
    public SortDirectionType $direction;
    public CommentSortType $sort;

    public function getDirection(): string
    {
        return $this->direction->getValue();
    }

    public function getSort(): string
    {
        return $this->sort->getValue();
    }
}
