<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Company;

final class CompanyRequestDto implements \JsonSerializable
{
    private string $id;
    private string $name;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
