<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Product;

use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;

final class ProductRequestDto implements \JsonSerializable
{
    private CompanyRequestDto $companyRequestDto;
    private string $id;
    private string $name;

    public function __construct(CompanyRequestDto $companyRequestDto, string $id, string $name)
    {
        $this->companyRequestDto = $companyRequestDto;
        $this->id = $id;
        $this->name = $name;
    }

    public function jsonSerialize(): array
    {
        return [
            'company' => $this->companyRequestDto,
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
