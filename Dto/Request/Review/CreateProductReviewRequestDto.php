<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review;

use Slivki\Bundle\ReviewBundle\Dto\Request\Product\ProductRequestDto;

final class CreateProductReviewRequestDto implements \JsonSerializable
{
    private CreateReviewRequestDto $reviewRequestDto;
    private ProductRequestDto $productRequestDto;
    private string $reviewerToken;

    public function __construct(
        CreateReviewRequestDto $reviewRequestDto,
        ProductRequestDto $productRequestDto,
        string $reviewerToken
    ) {
        $this->reviewRequestDto = $reviewRequestDto;
        $this->productRequestDto = $productRequestDto;
        $this->reviewerToken = $reviewerToken;
    }

    public function jsonSerialize(): array
    {
        return [
            'product' => $this->productRequestDto,
            'reviewerToken' => $this->reviewerToken,
            'review' => $this->reviewRequestDto,
        ];
    }
}
