<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review;

use Slivki\Bundle\ReviewBundle\Dto\Request\Product\ProductRequestDto;

final class CreateListProductReviewRequestDto implements \JsonSerializable
{
    private CreateReviewRequestDto $reviewRequestDto;
    private array $productRequestDto;
    private string $reviewerToken;

    /**
     * @param ProductRequestDto[] $productsRequestDto
     */
    public function __construct(
        CreateReviewRequestDto $reviewRequestDto,
        array $productsRequestDto,
        string $reviewerToken
    ) {
        $this->reviewRequestDto = $reviewRequestDto;
        $this->productRequestDto = $productsRequestDto;
        $this->reviewerToken = $reviewerToken;
    }

    public function jsonSerialize(): array
    {
        return [
            'products' => $this->productRequestDto,
            'reviewerToken' => $this->reviewerToken,
            'review' => $this->reviewRequestDto,
        ];
    }
}
