<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review\Like;

final class LikeRequestDto implements \JsonSerializable
{
    private int $reviewId;
    private string $reviewerToken;

    public function __construct(int $reviewId, string $reviewerToken)
    {
        $this->reviewId = $reviewId;
        $this->reviewerToken = $reviewerToken;
    }

    public function jsonSerialize(): array
    {
        return [
            'reviewId' => $this->reviewId,
            'reviewerToken' => $this->reviewerToken,
        ];
    }
}
