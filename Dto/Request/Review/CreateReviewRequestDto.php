<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review;

final class CreateReviewRequestDto implements \JsonSerializable
{
    private string $text;
    private int $rating;
    private array $imageUrls;

    public function __construct(string $text, int $rating, array $imageUrls)
    {
        $this->text = $text;
        $this->rating = $rating;
        $this->imageUrls = $imageUrls;
    }

    public function jsonSerialize(): array
    {
        return [
            'text' => $this->text,
            'rating' => $this->rating,
            'imageUrls' => $this->imageUrls,
        ];
    }
}
