<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review;

use Slivki\Bundle\ReviewBundle\Dto\Request\Company\CompanyRequestDto;

final class CreateCompanyReviewRequestDto implements \JsonSerializable
{
    private CreateReviewRequestDto $reviewRequestDto;
    private CompanyRequestDto $companyRequestDto;
    private string $reviewerToken;

    public function __construct(
        CreateReviewRequestDto $reviewRequestDto,
        CompanyRequestDto $companyRequestDto,
        string $reviewerToken
    ) {
        $this->reviewRequestDto = $reviewRequestDto;
        $this->companyRequestDto = $companyRequestDto;
        $this->reviewerToken = $reviewerToken;
    }

    public function jsonSerialize(): array
    {
        return [
            'company' => $this->companyRequestDto,
            'reviewerToken' => $this->reviewerToken,
            'review' => $this->reviewRequestDto,
        ];
    }
}
