<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review;

final class RemoveReviewRequestDto implements \JsonSerializable
{
    private string $reviewerToken;

    public function __construct(string $reviewerToken)
    {
        $this->reviewerToken = $reviewerToken;
    }

    public function jsonSerialize(): array
    {
        return [
            'reviewerToken' => $this->reviewerToken,
        ];
    }
}
