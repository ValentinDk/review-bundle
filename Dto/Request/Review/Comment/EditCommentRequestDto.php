<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment;

final class EditCommentRequestDto implements \JsonSerializable
{
    private string $reviewerToken;
    private string $text;

    public function __construct(string $reviewerToken, string $text)
    {
        $this->reviewerToken = $reviewerToken;
        $this->text = $text;
    }

    public function jsonSerialize(): array
    {
        return [
            'reviewerToken' => $this->reviewerToken,
            'text' => $this->text,
        ];
    }
}
