<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment;

final class CreateCommentRequestDto implements \JsonSerializable
{
    private int $reviewId;
    private string $reviewerToken;
    private ?int $parentId;
    private string $text;

    public function __construct(
        int $reviewId,
        string $reviewerToken,
        ?int $parentId,
        string $text
    ) {
        $this->reviewId = $reviewId;
        $this->reviewerToken = $reviewerToken;
        $this->parentId = $parentId;
        $this->text = $text;
    }

    public function jsonSerialize(): array
    {
        return [
            'reviewId' => $this->reviewId,
            'reviewerToken' => $this->reviewerToken,
            'parentId' => $this->parentId,
            'text' => $this->text,
        ];
    }
}
