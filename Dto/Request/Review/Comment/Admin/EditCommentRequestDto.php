<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Dto\Request\Review\Comment\Admin;

final class EditCommentRequestDto implements \JsonSerializable
{
    private string $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function jsonSerialize(): array
    {
        return [
            'text' => $this->text,
        ];
    }
}
