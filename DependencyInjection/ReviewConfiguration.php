<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class ReviewConfiguration implements ConfigurationInterface
{
    private string $treeName;

    public function __construct(string $treeName)
    {
        $this->treeName = $treeName;
    }

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder($this->treeName);
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('username')->isRequired()->end()
                ->scalarNode('password')->isRequired()->end()
                ->scalarNode('base_uri')->isRequired()->end()
            ->end();

        return $treeBuilder;
    }
}
