<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

final class ReviewExtension extends ConfigurableExtension
{
    private const ALIAS = 'review';

    public function getConfiguration(array $config, ContainerBuilder $container): ReviewConfiguration
    {
        return new ReviewConfiguration(self::ALIAS);
    }

    public function getAlias(): string
    {
        return self::ALIAS;
    }

    public function loadInternal(array $mergedConfig, ContainerBuilder $container): void
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $this->configureHttpClient($mergedConfig, $container);
    }

    public function configureHttpClient(array $config, ContainerBuilder $container): void
    {
        $container->setParameter('review.http_client.username', $config['username']);
        $container->setParameter('review.http_client.password', $config['password']);
        $container->setParameter('review.http_client.base_uri', $config['base_uri']);
    }
}
