<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\DependencyInjection;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class ReviewBundle extends Bundle
{
    public function getContainerExtension(): ReviewExtension
    {
        return new ReviewExtension();
    }
}
