<?php

declare(strict_types=1);

namespace Slivki\Bundle\ReviewBundle\Serializer\Config;

final class SerializerConfig
{
    public const JSON = 'json';
}
